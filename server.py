import sqlite3 
db = sqlite3.connect("file:bot/database.db", uri=True, check_same_thread=False)
db.row_factory = sqlite3.Row


class execute():
    def __init__(self, command, params=tuple(), commit=True):
        c = db.cursor()
        c.execute(command, params)
        self.data = [dict(el) for el in list(c)]
        self.isEmpty = False
        if len(self.data) == 0:
            self.isEmpty = True
        if commit:
            db.commit()
    def __getitem__(self, v):
        return self.data[v];



def getUidByKey(key):
    u = execute("select uid from regLinks where key=?", (key, ))
    if u.isEmpty: return -1
    return u[0]["uid"]

def getStudentByUid(uid):
    return execute("select * from student where uid=?", (uid, ))

from bottle import get, post, request, static_file, run, auth_basic, abort, template, redirect
import json

@get('/')
def main_page():
    return static_file("landing.html", root="src")

@get('/assets/<filename:path>')
def assets(filename):
    return static_file(filename, root='src/assets')

def checkUid(key):
    uid = getUidByKey(key)
    if uid == -1: abort(404)
    return uid

f = open('src/register.html', 'r')
registerPage = f.read()
emptyFill = {'name': '', 'surname': '', 'phone': '', 'school': '', 'grade': ''}
f.close()

@get('/register/<key>')
def reg_page(key):
    uid = checkUid(key)
    userExist = not execute("select uid from enrolledStudents where uid=?", (uid, )).isEmpty
    if not userExist: return template(registerPage, emptyFill)
    return static_file("success.html", root="src")

@get('/register/<key>/edit')
def edit_reg_page(key):
    uid = checkUid(key)
    data = execute("select name, surname, school, phone, grade from enrolledStudents where uid=?", (uid, ))[0]
    return template(registerPage, data)

@post('/register')
def reg_page_api():
    p = request.params
    uid = getUidByKey(p.key)
    if uid == -1: abort(405)
    userExist = not execute("select uid from enrolledStudents where uid=?", (uid, )).isEmpty
    
    if not userExist:
        execute("""insert into enrolledStudents (uid) values (?) """, (uid, ))
        
    execute("""update enrolledStudents set
        name=?, surname=?, school=?, phone=?, grade=?
        where uid=?
    """, (p.name, p.surname, p.school, p.phone, p.grade, uid))
    
    return p.key

def check_pass(username, password):
    return username == 'itschool' and password == 'black mongoose cum with green sperm'

@get('/scoreboard')
@auth_basic(check_pass)
def scoreboard():
    return static_file('scoreboard/index.html', root='src')

@get('/scoreboard/<filename:path>')
@auth_basic(check_pass)
def scoreboard_assets(filename):
    return static_file(filename, root='src/scoreboard')

@get('/scoreboard/data')
@auth_basic(check_pass)
def getData():
    users = execute('select * from students where examFinished=1 order by score DESC, timeRest ASC').data
    tasks = execute('select * from tasks').data
    forms = execute('select * from enrolledStudents').data
    return json.dumps({'users': users, 'tasks': tasks, 'forms': forms})

run(host='0.0.0.0', port=8000)
