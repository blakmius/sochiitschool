// ZOMBULAR ed. 8, 2017-02-26, ML
define(['./cito'], function(cito) {

var rootNode, pending = false, cbs = [], updating = false, context = {};
var body;

function performUpdate() {
    if (typeof body !== 'function') { // not ready to update
        pending = false; 
        return; 
    }

    updating = true;
    if (rootNode === undefined) {
        rootNode = cito.vdom.append(document.body, body(context));
    } else {
        cito.vdom.update(rootNode, body(context));
    }        
    pending = false;
    while (cbs.length > 0) {
        var cbq = cbs;
        cbs = [];
        for (var i = 0; i < cbq.length; i++) { cbq[i](); }
    }
    context = {};
    updating = false;
}
// call it every time when changes have to be applied into DOM
function update(cb) {
    // if (updating) console.error('In the process of updating, update() should not be called.\nYou might want to consider calling afterUpdate(cb) instead.');
    if (cb === true) { 
        performUpdate(); 
    } else {
        if (typeof cb === 'function') cbs.push(cb);
        else for (var k in cb) if (cb.hasOwnProperty(k)) context[k] = cb[k];

        if (!pending) setTimeout(performUpdate, 0);
        pending = true;
    }
}
// call it when no update is needed, and you only need to deal with post-update DOM
function afterUpdate(cb) {
    if (!updating) { // Actially, it is a warning, as everything works ok
        // console.error is used so that the programmer could see a traceback
        console.error('The function afterUpdate(cb) should be called during the update process.');
        update(cb);
    } else {
        cbs.push(cb);
    }
}

function fval(f, a) { return (typeof f === 'function') ? f(a) : f; }
function normChild(c, ctx) {
    if (typeof c === 'function') c = normChild(c(ctx), ctx);
    if (Array.isArray(c)) c = Array.prototype.concat.apply([], c.map(i => normChild(i, ctx)));
    if (c === undefined || c === null) return '';
    if (typeof c === 'number') return String(c);
    return c;
}
function toStringList(v, ctx) {
    if (v !== 0 && !v) return [];
    else if (typeof v === 'string') return [v];
    else if (typeof v === 'function') return toStringList(v(ctx), ctx);
    else if (typeof v === 'object') {
        if (Array.isArray(v)) return Array.prototype.concat.apply([], v.map(i => toStringList(i, ctx)));
        else {
            var result = [];
            for (k in v) if (v.hasOwnProperty(k)) {
                if (fval(v[k])) result.push(k);                    
            }
            return result;
        }
    } 
    else return [String(v)];
}
function c(spec) {
    var specre = /(^([0-9a-z\-_]+)|[<!])|([#\.]?[0-9a-z\-_]+)/gi;
    var children = Array.prototype.slice.call(arguments, 1);
    return function(ctx) {
        var result = {attrs: {}, events: {}}, vspc = fval(spec, ctx), cls = [], id;
        var tagspec = (typeof vspc === 'object') ? fval(vspc.is, ctx) : vspc;
        if (tagspec) { 
            result.tag = 'div';
            (tagspec.match(specre) || []).forEach(function (m) {
                if (m.charAt(0) == '.') cls.push(m.substr(1));
                else if (m.charAt(0) == '#') id = m.substr(1);
                else result.tag = m;
            });
        }
        
        if (id) result.attrs.id = id;
        
        if (typeof vspc === 'object') for (var key in vspc) 
        if (vspc.hasOwnProperty(key) && (key !== 'is')) {
            if (key.substr(0, 2) === 'on' && vspc[key]) { 
                result.events[key.substr(2)] = vspc[key];
            } else {            
                var nval = fval(vspc[key], ctx);
                if (nval === undefined) continue;
                if (key === 'key') {
                    result.key = nval;
                } else if (key === 'class') {
                    cls = cls.concat(toStringList(vspc[key], ctx));
                } else {
                    result.attrs[key] = nval;
                }
            }
        }
        
        if (cls.length > 0) result.attrs['class'] = cls.join(' ');            
        if (children.length > 0) result.children = normChild(children, ctx); // children.map(function(c) { return fval(c, ctx) || ''; });
        return result;
    };
}

function each(list, func, between) {
    var result = [];
    for (var i in list) if (list.hasOwnProperty(i)) {
        var item = func(list[i], i);
        if (item != null) {
            result.push(item);
            if (between != undefined) result.push(between);
        }
    }
    if (between != undefined) result.pop();
    return result;
}

function wrap(cl) {
    return {get: cl.get.bind(cl), set: cl.set.bind(cl)};
}
function Val(v) {
    return wrap({
      v: v,
      get: function() { return this.v; },
      set: function(w) { return this.v = w; }
    });
}
function nf(f) { return (typeof f === 'function') ? f() : f; }
function Ref(env, name) {
    return wrap({
      env: env,
      name: name,
      get: function() { return nf(this.env)[this.name]; },
      set: function(value) { return nf(this.env)[this.name] = value; }
    });
}

function setBody(b) { body = b; update(); }

c.update = update;
c.afterUpdate = afterUpdate;
c.setBody = setBody;
c.each = each;
c.Val = Val;
c.Ref = Ref;

c.c = c;
return c;

// TODO: free the system from the totalitarian rule of update() linked to `body`
// this may require to enable components to somehow update, i.e. redraw themselves

});

