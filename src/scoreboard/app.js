require(['zombular'], function (z) {
	class User {
		constructor({uid, stage, name, taskNumber, score, registerTime, timeRest}) {
			this.name = name;
			this.stage = stage;
			this.score = score;
			this.timeRest = timeRest;
			this.uid = uid;
			this.registerTime = registerTime;
			this.color = randomInt(1, 8);

			this.tasks = [];
			let _tasksIndex = []

			tasks.forEach((t, i) => {
				if (t.uid == this.uid) {
					t.isCorrect = t.answer == t.userAnswer;
					this.tasks.push(t);
					_tasksIndex.push(i)
				}
			});

			for (var i = _tasksIndex.length - 1; i >= 0; i--) {
				tasks.splice(_tasksIndex[i], 1);
			}

			this.tasks.sort((a, b) => a.taskNumber - b.taskNumber);

			this.form = forms.find(i => i.uid == this.uid);
		}

		get initials() {
			let initials = this.name.match(/\b\w/g) || [];
			return ((initials.shift() || '') + (initials.pop() || '')).toUpperCase();
		}
	}

	function randomInt(min, max) {
  		min = Math.ceil(min);
  		max = Math.floor(max);
  		return Math.floor(Math.random() * (max - min)) + min;
	}

	function getJSON(url, callback) {
	    const xhr = new XMLHttpRequest();
	    xhr.open('GET', url, true);
	    xhr.responseType = 'json';
	    xhr.onload = function() {
	      var status = xhr.status;
	      if (status == 200) {
	        callback(null, xhr.response);
	      } else {
	        callback(status);
	      }
	    };
	    xhr.send();
	};

	let users = [],
		tasks = [],
		forms = [];

	var curUser;

	getJSON('/scoreboard/data', (err, data) => {
		tasks = data.tasks;
		forms = data.forms;
		users = data.users.map(u => new User(u));
		z.update();
	})

	var Titled = (title, what, is='div') =>
      z(is, z('.l1.uc.c1.e.nosel', title), what);

	z.setBody(z('',
		z('.scoreboard',
			z('.head', z('.sz3-63', 'Place'), z('.sz10', 'Name'), z('div', 'Score')),
			() => users.map((user, place) => z('div.user',
				z('.sz3', '#'+ ++place),
				z({is: 'span.photo .bgcolor_'+user.color, 'data-content': user.initials}),
				z({is: 'a.name .sz10', onclick: e=>{curUser=user;z.update();}}, user.name),
				z('.points.sz3', user.score))
			)),
		() => curUser ? z('.protocol',
			z('h1', 'Form'),
			z('.g',
				Titled('name', curUser.form.name),
				Titled('surname', curUser.form.surname)
			),
			Titled('phone', curUser.form.phone),
			z('.g',
				Titled('school', curUser.form.school),
				Titled('grade', curUser.form.grade)
			),
			z('h1', 'Protocol'),
			Titled('name', curUser.name),
			z('.g',
				Titled('stage', curUser.stage),
				Titled('points', curUser.score),
				Titled('timeRest', curUser.timeRest)
			),
			Titled('Register Date', curUser.registerTime),
			curUser.tasks.map((t, i) => z('.task',
				z('h3.ts', 'Task #'+t.taskNumber),
				z('.g',
					Titled('points', t.score),
					Titled('taskNumber', t.taskNumber),
					Titled('isCorrect', String(t.isCorrect))
				),
				Titled('Date', t.date),
				z('.text', t.text),
				z('.g',
					z({is: 'div', class: {error: !t.isCorrect, success: t.isCorrect}}, 'User answer: ' + t.userAnswer),
					z('div', 'Correct answer: ' + t.answer)
				)
			))
		) : ''
	));
});
