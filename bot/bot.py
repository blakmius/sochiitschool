import sys
import time
import telepot

SITE = '0.0.0.0:8080'

"""
D A T A B A S E
"""
import sqlite3

db = sqlite3.connect("database.db", uri=True, check_same_thread=False)
db.row_factory = sqlite3.Row

db.execute("""
    create table if not exists enrolledStudents (
        uid integer primary key,
        school integer,
        phone varchar(15),
        name varchar(80),
        surname varchar(80),
        grade integer
    );
""")


db.execute("""
    create table if not exists students (
        uid integer primary key,
        stage integer,
        name varchar(80),
        taskNumber integer,
        timeRest integer,
        score integer,
        registerTime integer,
        examFinished integer
    );
""")


db.execute("""
    create table if not exists tasks (
        uid integer,
        taskNumber integer,
        text varchar(300),
        answer varchar(80),
        userAnswer varchar(80),
        isCorrect integer,
        score integer
    );
""")

db.execute("""
    create table if not exists regLinks (
        uid integer,
        key varchar(80)
    );
""")

class execute():
    def __init__(self, command, params=tuple(), commit=True):
        c = db.cursor()
        c.execute(command, params)
        self.data = [dict(el) for el in list(c)]
        self.isEmpty = False
        if len(self.data) == 0:
            self.isEmpty = True
        if commit:
            db.commit()
    def __getitem__(self, v):
        return self.data[v];


"""
H E L P E R S
"""
import time
import string
import random
alphabet = string.ascii_lowercase
def randomUniqueString():
    return "".join([alphabet[random.randint(0, len(alphabet) - 1)] for i in range(10)])

def thisStudentExists(uid):
    return not execute("select uid from students where uid=?", (uid,)).isEmpty

def getStudentInfo(uid):
    return execute("select * from students where uid=?", (uid,)).data[0]

def regStudent(student):
    fname = student["first_name"] if "first_name" in student else "?"
    lname = student["last_name"] if "last_name" in student else "?"
    name = "{0} {1}".format(fname, lname)
    execute("""
        insert into students
        (uid, name, stage, taskNumber, registerTime, score, timeRest)
        values (?, ?, ?, ?, ?, ?, ?)
    """, (student["id"], name, 0, 0, int(time.time()), 0, 0))
    return getStudentInfo(student["id"])
    
def updateTable(values, tableName="", keys=[], primaryKey=""):
    query = ""
    for key in keys: query += "{} = {}".format(key, values[key])
    execute("""
        update {} set {} where {}=?;  
    """.format(tableName, query, primaryKey), (values[primaryKey],))

def thisAnswerCorrect(student, answer):
    return not execute("select * from tasks where uid=? and taskNumber=? and answer=?", (student["uid"], student["taskNumber"], answer)).isEmpty

def getCurrentTaskInfo(student):
    return execute("select * from tasks where uid=? and taskNumber=?", (student["uid"], student["taskNumber"]))[0];

def addNewTask(student):
    student["taskNumber"] += 1
    updateTable(student, tableName="students", keys=["taskNumber"], primaryKey="uid")
    task = generateTask(student)
    execute("insert into tasks (uid, taskNumber, answer, text, score) values (?, ?, ?, ?, ?)",
        (task["uid"], task["taskNumber"], task["answer"], task["text"], task["score"]))
    return task
    
def updateTimeRest(student):
    timeRest = int(time.time() - student["registerTime"])
    return execute("update students set timeRest=? where uid=?", (timeRest, student["uid"]))

def onCorrectAnswer(student, task):
    execute("""
        update students set score=? where uid=?
    """, (student["score"] + task["score"], student["uid"]))

"""
S T A G E S
"""
from utils import *

def currentStage(state):
    student = state["student"]
    stages[student["stage"]](state)

def welcome(state):
    keyboard = Keyboard([
        [KeyboardButton("НАЧАТЬ")]
    ])
    msg = """
Здесь вы можете пройти отбор в сочинскую Школу Программиста."""
    bot.sendMessage(state["chat_id"], msg, parse_mode="HTML", reply_markup=keyboard)
    
    
def start(state):
    keyboard = Keyboard([
        [KeyboardButton("Получить задания!")]
    ])
    msg ="""
Вы получите список из <b>10 заданий и 5 теоретических вопросов</b> для проверки ваших знаний в основах программирования.
Постарайтесь потратить не очень много времени на прохождение этого экзамена.
Для выполнения заданий вам потребуются базовые знания циклов, ветвлений, переменных и логических операций. 
Задания будут оформлены в виде программ на языке программирования <b>Python</b>. Удачи!
"""
    bot.sendMessage(state["chat_id"], msg, parse_mode="HTML", reply_markup=keyboard)
   
   
from taskGenerator import generateTask 
def tasks(state):
    student, answer = state["student"], state["msg_text"]
    taskN = student["taskNumber"]

    if taskN > 0:
        currentTask, correct = getCurrentTaskInfo(student), thisAnswerCorrect(student, answer)
        execute("""
            update tasks set isCorrect=?, userAnswer=? where uid=? and taskNumber=?;  
        """, (int(correct), answer, student["uid"], taskN))
        
        
        
        updateTimeRest(student)
        
        
        if correct:
            onCorrectAnswer(student, currentTask)
            bot.sendMessage(state["chat_id"], "✅ Правильный ответ!", reply_markup={"remove_keyboard": True})
        else:
            msg = """⛔Вы ответили неверно :( \nПравильный ответ: {}
            """.format(currentTask["answer"])
            bot.sendMessage(state["chat_id"], msg, parse_mode="HTML", reply_markup={"remove_keyboard": True})
        if taskN >= 15:
            student["stage"] += 1
            updateTable(student, "students", keys=["stage"], primaryKey="uid")
            return currentStage(state)
            
    task = addNewTask(student)
    msg = """
    <b>Задание №{}</b>\n{}
    """.format(taskN + 1, task["text"])
    
    keyboard = task["keyboard"] if "keyboard" in task else {"remove_keyboard": True}
    bot.sendMessage(state["chat_id"], msg, parse_mode="HTML", reply_markup=keyboard)
    
def register(state):
    link = randomUniqueString()
    execute("insert into regLinks (uid, key) values(?, ?)", (state["student"]["uid"], link))
    execute("update students set examFinished=1 where uid=?", (state["student"]["uid"], ))
    
    bot.sendMessage(state["chat_id"], "Ссылка для регистрации на нашем сайте: "+SITE+"/register/" + str(link), parse_mode="HTML", reply_markup={"remove_keyboard": True})

def gtfo(state):
    link = execute("select key from regLinks where uid=?", (state["student"]["uid"], ))[0]["key"]
    bot.sendMessage(state["chat_id"], "Ссылка для регистрации на нашем сайте: "+SITE+"/register/" + str(link), parse_mode="HTML", reply_markup={"remove_keyboard": True})

stages = [
    welcome,
    start,
    tasks,
    register,
    gtfo
]
def DO_SOMETHING(state):
    student, showMessage, msg = state["student"], False, state["msg_text"]
    def goNextStage():
        student["stage"] += 1
        updateTable(student, "students", keys=["stage"], primaryKey="uid")
        currentStage(state)
    
    if   msg == "/start": currentStage(state);    
    elif student["stage"] == 0 and msg == "НАЧАТЬ": goNextStage();
    elif student["stage"] == 1 and msg == "Получить задания!": goNextStage();
    elif student["stage"] in (2, 4): currentStage(state);
    elif student["stage"] == 3: goNextStage()

    
    

    
"""
B O T
"""
def handle(msg):    
    content_type, chat_type, chat_id = telepot.glance(msg)
    student = msg["from"]
    state = {}
    if thisStudentExists(student["id"]):
        state["student"] = getStudentInfo(student["id"])
    else:
        state["student"] = regStudent(student)
    state["msg_text"], state["msg_date"], state["chat_id"] = msg["text"], msg["date"], chat_id
    #print(msg["text"])

    DO_SOMETHING(state)

TOKEN = sys.argv[1]  # get token from command-line

bot = telepot.Bot(TOKEN)
bot.message_loop(handle)
print ('Listening ...')

# Keep the program running.
while 1:
    time.sleep(10)
