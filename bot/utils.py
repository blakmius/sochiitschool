import random
def APPLY(object1, object2):
    for key in object2: object1[key] = object2[key]
    return object1

def KeyboardButton(text="empty message", **args):
    return APPLY({"text": text}, args)
    
def Keyboard(buttons, t="keyboard", **args):
    return APPLY({t: buttons}, args)

def RandomKeyboard(buttons, **args):
    for l in buttons: random.shuffle(l)
    random.shuffle(buttons)
    return Keyboard(buttons, **args)
