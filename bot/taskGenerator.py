import string
import random
import re
from utils import *
operator = lambda: random.choice("/*-+")
num = lambda: random.choice(range(4, 50))
lettersStr = string.ascii_lowercase
for s in "oOIl": lettersStr = lettersStr.replace(s, "")

def randomUnique(l):
    i = random.choice(range(len(l)))
    el = l[i]; del l[i];
    return el
    
def variables():
    letters = list(lettersStr)
    letter = lambda: randomUnique(letters)
    
    a, b, c = letter(), letter(), letter()
    av, bv, cv = num(), num(), num()
    op1, op2, op3 = operator(), operator(), operator()
    if op1 == "/": op1 = "-"
    if op2 == "/": op2 = "-"
    if op3 == "/": op3 = "-"
    program =  """{a} = {av}
{b} = {a} {op1} {bv} {op2} {cv}
{c} = {b} {op3} {a}
print({c})
""".format(**locals())
    v2 = eval("{av} {op1} {bv} {op2} {cv}".format(**locals()))
    v3 = eval("{v2} {op3} {av}".format(**locals()))
    
    return {
        "text": "Тема задачи: <i>работа с переменными</i>\n\nКакое число выведет на экран эта программа?\n<code>" + program + "</code>",
        "answer": str(v3),
        "score": 1
    }

def if_statements():
    letters = list(lettersStr)
    letter = lambda: randomUnique(letters)
    
    a, c = letter(), letter()
    av, bv, cv, dv = num(), num(), num(), num()
    op1, op2 = operator(), operator()
    if av > bv: ans = cv + dv
    else: ans = cv - dv
    program = """{a} = {av}
{c} = {cv}
if {a} > {bv}:
  {c} = {c} + {dv}
else:
  {c} = {c} - {dv}
print({c})
""".format(**locals())
    
    return {
        "text": "Тема задачи: <i>ветвления</i>\n\nКакое число выведет на экран эта программа?\n<code>" + program + "</code>",
        "answer": str(ans),
        "score": 3
    }
    
def whiles():
    letters = list(lettersStr)
    letter = lambda: randomUnique(letters)
    
    a, b = letter(), letter()
    av, bv, cv, dv = num(), num(), num() % 5 + 1, num() % 5 + 1
    op1, op2 = operator(), operator()
    ans = bv + cv * dv
    program = """{a} = {av}
{b} = {bv}
while {a} &lt; {av_cv_sum}:
  {b} = {b} + {dv}
  {a} = {a} + 1
print({b})
""".format(av_cv_sum=av+cv, **locals())
    
    return {
        "text": "Тема задачи: <i>циклы</i>\n\nКакое число выведет на экран эта программа?\n<code>" + program + "</code>",
        "answer": str(ans),
        "score": 3
    }
    
def complete_the_code1():
    letters = list(lettersStr)
    letter = lambda: randomUnique(letters)
    a, b = letter(), letter()
    av, bv = num(), num()
    op = operator()
    if op == "/": op = "*"
    v = str(eval("{av} {op} {bv}".format(**locals())))
    program = """{a} = {av}
?????
print({a} {op} {b})
""".format(**locals())
    
    ans = "{b} = {bv}".format(**locals())

    return {
        "text": "Выберите вариант кода, который нужно подставить вместо <b>?????</b> чтобы программа вывела на экран число <b>{}</b>.\n\nКод программы:\n<code>{}</code>".format(v, program),
        "keyboard": RandomKeyboard([
            [KeyboardButton("{b} = {av} + {bv}".format(**locals())),
                KeyboardButton("{b} = {av} - {bv}".format(**locals()))],
            [KeyboardButton(ans),
                KeyboardButton("{} = {}".format(b, bv * 2 - 1))]
        ], one_time_keyboard=True),
        "answer": ans,
        "score": 2
    }

    
def vars_switch():
    a = KeyboardButton("A = B; B = A")
    b = KeyboardButton("A = C; B = C")
    c = KeyboardButton("C = A; A = B; B = C")
    d = KeyboardButton("A = B; B = A; A = B")

    return {
        "text": "Какой из предложенных варинатов кода меняет значения переменных A и B?",
        "keyboard": RandomKeyboard([
            [a, b], [c, d]
        ], one_time_keyboard=True),
        "answer": c["text"],
        "score": 2
    }


def templatize(string):
    mathOp = ['+', '-', '*']
    compareOp = ['>', '<', '==', '<=', '>=', '!=']
    logicOp = ['and', 'or']
    choice = random.choice
    letters = list(lettersStr)
    letter = lambda: randomUnique(letters)

    vars = {}

    def replace(regex, func, string):
        for r in set(re.findall(regex, string)):
            vars[r] = func()
            string = string.replace(r, str(vars[r]))
        return string

    string = replace('\$var[0-9]+', letter, string)
    string = replace('\$mathOp[0-9]+', lambda: choice(mathOp), string)
    string = replace('\$compareOp[0-9]+', lambda: choice(compareOp), string)
    string = replace('\$logicOp[0-9]+', lambda: choice(logicOp), string)
    string = replace('\$num[0-9]+', num, string)

    return {'text': string, 'vars': vars}

def mix():
    template = templatize("""$var0 = $num0
$var1 = 0
while $var1 < $num1:
  $var1 = $var1 + 1

if $var1 $compareOp1 $var0:
  print($var0 $mathOp0 $var1)
else:
  print($var1 $mathOp1 $var0)
""")
    v = template['vars']
    answer = eval('{0} {1} {2}'.format(v['$num1'], v['$compareOp1'], v['$num0']))
    if answer: answer = eval('{0} {1} {2}'.format(v['$num0'], v['$mathOp0'], v['$num1']))
    else: answer = eval('{0} {1} {2}'.format(v['$num1'], v['$mathOp1'], v['$num0']))
    return {
        'text': "Тема задачи: <i>нет</i>\n\nКакое число выведет на экран эта программа?\n<code>" + template['text'].replace(">", "&gt;").replace("<", "&lt;") + "</code>",
        'answer': answer,
        "score": 4
    }

def complete_the_code2():
    template = templatize("""$var0 = $num0
$var1 = $num1
$var2 = $var1 + $var0
$var3 = $var1 - $var0
?????
print($var1*2 == $var4) # needs to be True
""")
    v, text = template['vars'], template['text']
    a, b, c = v['$var4'], v['$var2'], v['$var3']
    answer = '{a} = {b} + {c}'.format(a=a, b=b, c=c)    
    
    return {
        'text': """Выберите вариант кода, который нужно подставить вместо <b>?????</b> чтобы программа вывела на экран булево значение истины (True).

Код программы:
<code>{}</code>""".format(template["text"]), 
        'answer': answer, 
        'keyboard': RandomKeyboard([
            [KeyboardButton('{b} = {a} + {c}'.format(b=b, a=a, c=c)),
                KeyboardButton('{a} = {b} - {c}'.format(b=b, a=a, c=c))],
            [KeyboardButton('{a} = {b} - {c}'.format(b=b, a=a, c=num())),
                KeyboardButton(answer)],
        ], one_time_keyboard=True),
        "score": 3
    }

def theor1():
    template = templatize("""Тема задачи: <i>теор. вопросы (ошибки в программах)</i>

Почему при выполнении этого кода интерпретатор выдаст ошибку?
<code>
$var1 = $num1
$var2 = ($var1 $mathOp1 $num2) $mathOp2 $num3
print($var3)
</code>""")

    a = KeyboardButton("Переполнение буфера")
    b = KeyboardButton("Назакрытая скобка")
    c = KeyboardButton("Необъявленная переменная в функции print")
    d = KeyboardButton("Синтансическая ошибка")
    return {
        "text": template['text'],
        "answer": c["text"],
        "keyboard": RandomKeyboard([[a], [b], [c], [d]]),
        "score": 1
    }
    
def theor2():
    template = templatize("""Тема задачи: <i>теор. вопросы (время выполнения программ)</i>

Какая из этих программ будет выполняться дольше, если их обе запустить на одном и том же компьютере при одинаковых условиях?

<b>Программа #1:</b>
<code>$var1 = 300
i = 0
while i &lt; $var1:
    i = i + 1
</code>

<b>Программа #2:</b>
<code>$var1 = 100
i = 0
while i &lt; $var1:
  i = i + 1

while i &lt; $var1 * 2:
  i = i + 1
</code>""")

    a = KeyboardButton("Программа #1")
    b = KeyboardButton("Программа #2")
    c = KeyboardButton("Обе программы выполнятся с одинаковой скоростью")
    return {
        "text": template['text'],
        "answer": a["text"],
        "keyboard": Keyboard([[a], [b], [c]]),
        "score": 3
    }
    
def theor3():
    template = templatize("""Тема задачи: <i>теор. вопросы (нахождение ошибок)</i>

Какая из этих программ не выполнится из-за ошибки?

<b>Программа #1:</b>
<code>$var1 = $num1
$var2 = $num1
$var1 = ($num2 * ($num1 + $num3) - $num4 + $num5)
print($var1)
</code>

<b>Программа #2:</b>
<code>$var1 = 100
$var2 = $num1
$var1 = ($num2 * ($num1 - $num4 + $num5)
print($var1)
</code>""")

    a = KeyboardButton("Программа #1")
    b = KeyboardButton("Программа #2")
    c = KeyboardButton("Обе программы выполнятся")
    d = KeyboardButton("Обе программы не выполнятся")
    return {
        "text": template['text'],
        "answer": b["text"],
        "keyboard": RandomKeyboard([[a], [b], [c], [d]]),
        "score": 2
    }
    
def theor4():
    template = templatize("""Тема задачи: <i>теор. вопросы (выполнение программ)</i>

Какие из утверждений верны?

1) Программы выполняются построчно
2) Обычно вычисления происходят слева направо
3) Программа останавливается во время ввода пользователем значения с клавиатуры после функции ввода-вывода
4) Открытые в коде программы скобки закрывать необязательно
5) Открытые в коде программы кавычки закрывать обязательно
6) Бесконечный цикл в программе обязательно приведет к ошибке и завершению программы""")

    a = KeyboardButton("1, 2, 3, 5")
    b = KeyboardButton("2, 4, 6")
    c = KeyboardButton("3, 2, 5")
    d = KeyboardButton("2, 3, 4, 6")
    e = KeyboardButton("Все утверждения верны")
    return {
        "text": template['text'],
        "answer": a["text"],
        "keyboard": Keyboard([[a, b], [c, d], [e]]),
        "score": 1
    }

def theor5():
    def expr(l=0):
        if l == 0: return '({}) {} ({})'.format(n(expr(l+1)), op(), n(expr(l+1)))
        if l == 2: return n(bool())
        return random.choice([
            lambda: n(bool()),
            lambda: '({}) {} {}'.format(n(expr(l+1)), op(), n(bool())),
            lambda: '({}) {} ({})'.format(n(expr(l+1)), op(), n(expr(l+1))),
            lambda: '{} {} ({})'.format(n(bool()), op(), n(expr(l+1)))
        ])()
    bool = lambda: random.choice(['True', 'False'])
    n = lambda x: random.choice(['not({})'.format(x), x])
    op = lambda: random.choice(['and', 'or'])
    e = expr()
    answer = eval('"Истина" if e else "Ложь"'.format(e))
    text = e.replace('True', 'Истина').replace('False', 'Ложь').replace('and', 'и').replace('or', 'или').replace('not', 'не')

    a = KeyboardButton("Ложь")
    b = KeyboardButton("Истина")
    return {
        "text": """Тема задачи: <i>теор. вопросы (логические выражения)</i>

Чему равно это логическое выражение?

{}""".format(text),
        "answer": answer,
        "keyboard": RandomKeyboard([[a], [b]]),
        "score": 4
    }

tasksList = [
    vars_switch,
    variables,
    if_statements,
    whiles,
    whiles,
    mix,
    mix,
    complete_the_code1,
    complete_the_code2,
    complete_the_code2,
    theor1,
    theor2,
    theor3,
    theor4,
    theor5,
]

def generateTask(student):
    taskN =  student["taskNumber"]

    task = tasksList[taskN - 1]()
    return APPLY({
        "taskNumber": taskN,
        "uid": student["uid"]
    }, task)


